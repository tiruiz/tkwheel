//
//  ViewController.swift
//  tkWheel
//
//  Created by Tiana Ruiz on 8/14/18.
//  Copyright © 2018 tkLABS. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    // Values on Wheel
    var wheelValues = ["Tiana", "Jim", "Jose", "Walter", "Li-Ling"]
    let network = Network()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func mainButtonPushed(_ sender: UIButton) {
        if wheelValues.isEmpty {
            return;
        }
        
        // generate random number
        let randomVal = Int(arc4random_uniform(UInt32(wheelValues.count)))
        
        // Do stuff with random val
        print("Picked string: ", wheelValues[randomVal], "\n");
        
        // Delete picked element from wheel
        wheelValues.remove(at: randomVal)
        
        // Connect to network
        network.connect()
    }
}

